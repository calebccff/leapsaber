import de.voidplus.leapmotion.*;

Leap leap;
Song song;

Camera camera;

void setup() {
  fullScreen(P3D);
  rectMode(CENTER);
  
  leap = new Leap(this);
  camera = new Camera(new PVector(width/2, height/2-200, 100), new PVector(-90, 20));
  song = new Song();

  selectInput("Select a song zip file", "loadSong");
}

void draw() {
  camera.display();
  doLights();
  background(50);

  drawMap();

  song.tick(leap); //Does a song tick to update note positions etc
  leap.display();

  song.displayInfo();
}
/** Song Meta Functions **/
void loadSong(File file) {
  String path = file.getAbsolutePath();
  try{
    Unzip.unzip(path, "song");
  }catch(IOException e){
    println("Failed to unzip file");
    e.printStackTrace();
  }


  song.loadSong();
  song.start();
}

/** Rendering Functions **/

void drawMap() {
  pushMatrix();
  translate(width/2, height, 0);
  fill(2);
  strokeWeight(10);
  box(1000, 100, -5000);
  popMatrix();
}

void doLights() {
  directionalLight(150, 150, 150, -100, 100, -100);
  spotLight(255, 200, 200, width/2-200, height/2-200, 0, 1, 1, -1, radians(50), 1);
  spotLight(200, 200, 255, width/2+200, height/2-200, 0, -1, 1, -1, radians(50), 1);
  //ambientLight(255, 255, 255, width/2-200, height/2-200, 0);
  // directionalLight(255, 255, 255, 100, 50, -100);
}

void debugHandHitbox() {
  for (Hand theHand : leap.getHands()) {
    PVector hp = leap.getHandPos(theHand);
    PVector hd = leap.getNormalFromDirection(theHand);
    for(int i = 0; i < 400; i+= 50) {
        PVector t = hd.copy();
        t.setMag(i);
        t.add(hp);
        pushMatrix();
        translate(t.x, t.y, t.z);
        fill(255, 255, 0);
        noStroke();
        sphere(20);
        popMatrix();
    }
  }
}