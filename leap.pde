
final int LEAP_Z_OFFSET = -600;


//Leap motion with a few tweaks specific to this game
class Leap extends LeapMotion {

    Leap(PApplet p) {
        super(p);
    }

    PVector getHandPos(Hand h) {
        PVector p = h.getStabilizedPosition();
        return new PVector(p.x, p.y, p.z+LEAP_Z_OFFSET);
    }

    PVector getHandDirection(Hand h) {
        return new PVector(270-h.getPitch(), -h.getYaw()); //Y is actually Z
    }
    PVector getNormalFromDirection(Hand h) {
        PVector dir = new PVector(-h.getPitch(), h.getYaw()-90);
        float x = cos(radians(dir.y));
        float z = sin(radians(dir.y));
        float y = tan(radians(dir.x));
        return new PVector(x, y, z);
    }

    void display() {
        for (Hand h : getHands()) {
            PVector pos = getHandPos(h);      

            pushMatrix();
            translate(pos.x, pos.y, pos.z);
            PVector dir = getHandDirection(h);
            rotateX(radians(dir.x));
            rotateZ(radians(dir.y));
            
            fill(0, 255, 0);
            noStroke();
            sphere(20);
            strokeWeight(10);
            stroke((h.isLeft()?color(255, 0, 0):color(0, 0, 255)));
            line(0, 0, 0, 400);
            popMatrix();
        }
    }
}