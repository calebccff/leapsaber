

class Song {
    ArrayList<Note> notes = new ArrayList<Note>();

    boolean playing = false;
    double time = 0;
    float beats = 0;
    double startTime = 0;
    float bpm;
    int score = 0;

    Song() {
        
    }

    void loadSong() {
        Gson gson = new Gson();
        JsonArray _notes = new JsonParser().parse(String.join("", loadStrings("song/"+getNoteFileName()))).getAsJsonObject().getAsJsonArray("_notes");
        for (JsonElement n : _notes) {
            notes.add(new Note(gson.fromJson(n, Note.class)));
        }
        bpm = new JsonParser().parse(String.join("", loadStrings("song/info.dat"))).getAsJsonObject().getAsJsonPrimitive("_beatsPerMinute").getAsFloat();
    }

    String getNoteFileName() {
        JsonObject ob = new JsonParser().parse(String.join("", loadStrings("song/info.dat"))).getAsJsonObject();
        ob = ob.getAsJsonArray("_difficultyBeatmapSets").get(0).getAsJsonObject(); //<>//
        ob = ob.getAsJsonArray("_difficultyBeatmaps").get(0).getAsJsonObject();
        String fname = ob.getAsJsonPrimitive("_beatmapFilename").getAsString();
        println(fname);
        return fname;
    }

    void tick(Leap leap) {
        if(!playing) return;
        time = (millis() - startTime);
        beats = (float)(bpm*(time/1000f/60f));
        //displayInfo();
        

        for (Note n : notes) {
            float diff = (float)(n.time - beats);
            
            if (diff > -4 && diff < 1) {
                println(diff);
                n.render(beats, bpm);
                score += n.check(leap);
            }
        }

    }

    void displayInfo() {
        camera();
        hint(DISABLE_DEPTH_TEST);
        fill(255);
        text((float)time, 10, 10);
        text("SCORE: "+str(score), 10, 30);
        hint(ENABLE_DEPTH_TEST);
    }

    void start() {
        playing = true;
        startTime = millis();
        Player.play("song/song.egg");
    }
}

class Note{
    @SerializedName("_time")
    double time;
    @SerializedName("_lineIndex")
    int horizontalPos;
    @SerializedName("_lineLayer")
    int verticalPos;
    @SerializedName("_type")
    int hand;
    @SerializedName("_cutDirection")
    int cutDirection;

    boolean played = false;
    PVector pos = new PVector(0, 0, 0);
    float size;
    color col = 0;

    Note(Note n) { //width and height are null without this.. I have NO IDEA
        //Somthing to do with the notes being created in the context of GSon instead of PApplet, hend creating a new note from the gson generated notes
        this.time = n.time;
        this.horizontalPos = n.horizontalPos;
        this.verticalPos = n.verticalPos;
        this.hand = n.hand;
        this.cutDirection = n.cutDirection;
    }

    //Checks to see if either Leap hand is hitting this note
    //TODO: Check velocity too (hand.getDynamics()?) - and possibly direction
    int check(Leap leap) {
        for (Hand theHand : leap.getHands()) {
            PVector hp = leap.getHandPos(theHand);
            PVector hd = leap.getNormalFromDirection(theHand);
            for(int i = 0; i < 400; i+= 50) {
                PVector t = hd.copy();
                t.setMag(i);
                t.add(hp);
                if (dist(t.x, t.y, pos.x, pos.y) < size && t.z-pos.z < 20) {
                    hit();
                    return 100;
                }
            }
        }
        return 0;
    }

    void hit() {
        played = true;
        println("HIT!");
    }

    void render(float beats, float bpm) {
        if(played) return;
        size = width*0.05;

        switch(HorizontalPosition.values()[horizontalPos]) {
            case Left:
                pos.x = width*0.3;
                break;
            case CenterLeft:
                pos.x = width*0.45;
                break;
            case CenterRight:
                pos.x = width*0.55;
                break;
            case Right:
                pos.x = width*0.7;
                break;
        }
        switch(VerticalPosition.values()[verticalPos]) {
            case Top:
                pos.y = height*0.6;
                break;
            case Middle:
                pos.y = height*0.5;
                break;
            case Bottom:
                pos.y = height*0.4;
                break;
        }
        //println(hand);
        switch(BSHand.values()[hand]) {
            case Left:
                col = color(240, 100, 100);
                break;
            case Right:
                col = color(100, 100, 240);
                break;
            case Bomb:
                col = color(255);
                break;
            default :
                col = color(0, 255, 0);
                break;
        }
        float timeToHit = (float)(beats-time);
        pos.z = timeToHit/bpm*60*500-1000;
        //println(pos, ":: ", timeToHit);

        fill(col);
        noStroke();
        pushMatrix();
        translate(pos.x, pos.y, pos.z);
        box(size);
        popMatrix();
    }
}

//Enums to represent note information
public enum BSHand
{
    Left         (0),
    Right        (1),
    None         (2),
    Bomb         (3);
    
    int val;
    BSHand(int h){val = h;}
}
public enum CutDirection
{
    Up           (0),
    Down         (1),
    Left         (2),
    Right        (3),
    UpLeft       (4),
    UpRight      (5),
    DownLeft     (6),
    DownRight    (7),
    Any          (8);
    
    int val;
    CutDirection(int h){val = h;}
}
public enum HorizontalPosition
{
    Left         (0),
    CenterLeft   (1),
    CenterRight  (2),
    Right        (3);
    
    int val;
    HorizontalPosition(int h){val = h;}
}
public enum VerticalPosition
{
    Bottom (0),
    Middle (1),
    Top (2);
    
    int val;
    VerticalPosition(int h){val = h;}
}
