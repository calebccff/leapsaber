

// class LeapHand {

//     void display(Hand h) {
//         PVector pos = h.getStabilizedPosition();
//         pos.z -= 600;        

//         pushMatrix();
//         translate(pos.x, pos.y, pos.z);
//         PVector dir = getFixedDirection(h);
//         rotateX(radians(dir.x));
//         rotateZ(radians(dir.y));
//         //rotateZ(radians(dir.z));
//         fill(0, 255, 0);
//         noStroke();
//         sphere(20);
//         strokeWeight(10);
//         stroke((h.isLeft()?color(255, 0, 0):color(0, 0, 255)));
//         line(0, 0, 0, 400);
//         popMatrix();
//     }

//     PVector getFixedDirection(Hand h) {
//         return new PVector(270-h.getPitch(), -h.getYaw()); //Y is actually Z
//     }

//     PVector getNormalFromDirection(Hand h) {
//         PVector dir = new PVector(-h.getPitch(), h.getYaw()-90);
//         float x = cos(radians(dir.y));
//         float z = sin(radians(dir.y));
//         float y = tan(radians(dir.x));
//         return new PVector(x, y, z);
//     }
// }